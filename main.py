import csv
import os
from os.path import exists
from os import remove

def search_character_id_in_csv(filename):
    id_character = 1
    with open(filename) as file:
        reader = csv.DictReader(file)
        for line in reader:
            id_character = int(line['id']) +1
    return id_character    

def create_character(filename, name, intelligence, power, strength, agility):

    fieldnames = ['id','name','intelligence','power','strength','agility']
    with open(filename, 'a') as file:
        writer = csv.DictWriter(file, fieldnames=fieldnames)

        id_character = search_character_id_in_csv(filename)
        #Add header, if archive have not size
        if not os.stat(filename).st_size:
            writer.writeheader()    
        character = {'id': str(id_character), 'name': name, 'intelligence': str(intelligence),\
            'power': str(power), 'strength': str(strength), 'agility': str(agility)}

        writer.writerow(character)
        return character


def find_character_by_id(filename, character_id):
    
    with open(filename, 'r+') as file:
        reader = csv.DictReader(file)
        for item in reader:
            if ( str(item['id']) == str(character_id)):
                return item
    return None

def find_all_characters(filename):
    result = []
    with open (filename, 'r+') as file:
        reader = csv.DictReader(file)

        for line in reader:
            for key, value in line.items():
                if (type(value) == str):
                    if(key == 'name'):
                        continue
                    line[key] = str(value)
            result.append(line)

    return result

def delete_character(filename, character_id):
    character_to_be_deleted = find_character_by_id(filename, str(character_id))
    if (character_to_be_deleted == None):
        return None
    result = []  
    with open(filename) as file:
        reader = csv.DictReader(file)
        for old_lines in reader:
            if(old_lines == character_to_be_deleted):
                continue
            result.append(old_lines)
    if exists(filename):
        remove(filename)
    for item in result:
        name = str(item['name'])
        intelligence = str(item['intelligence'])
        power = str(item['power'])
        strength = str(item['strength'])
        agility = str(item['agility'])
        result = create_character(filename, name, intelligence, power, strength, agility)

    if result:
        return True

def update_character(filename, character_id, **kwargs):
    character_to_update = find_character_by_id(filename, character_id)
    if (character_to_update == None):
        return None
    list_of_new_characters = []
    old_characters = find_all_characters(filename)
    for old_character in old_characters:
        if (old_character == character_to_update):
            for key, value in kwargs.items():
                if(old_character.get(key) == None):
                    continue
                old_character[key] = str(value)
                character_to_update = old_character
            list_of_new_characters.append(old_character)
        list_of_new_characters.append(old_character)
    if exists(filename):
        remove(filename)
    for character in list_of_new_characters:
        result = create_character(filename, str(character['name']), str(character['intelligence']), str(character['power']),\
             str(character['strength']), str(character['agility']))
    return character_to_update