from main import create_character, find_character_by_id, find_all_characters, delete_character, update_character

def test_create_character():
    filename = "characters.csv"
    name = "Huck"
    intelligence = '0'
    power = '7'
    strength = '10'
    agility = '8'

    expected = {
        "id": '1',
        "name": "Huck",
        "intelligence": '0',
        "power": '7',
        "strength": '10',
        "agility": '8'
    }

    result = create_character(filename, name, intelligence, power, strength, agility)
    #Rodar sem o arquivo characters, primeira vez
    #assert result == expected

def test_find_character_by_id():
    filename = "characters.csv"
    expected = {
        "id": '1',
        "name": "Huck",
        "intelligence": '0',
        "power": '7',
        "strength": '10',
        "agility": '8'
    }

    character_id = '1'
    result = find_character_by_id(filename, character_id) 
    #Pode rodar quantas vezes quiser
    #Se nao tiver ID no csv retorna None
    #assert result == expected 

def test_find_all_characters():
    filename = "characters.csv"
    expected = [{
    "id": 1,
    "name": "Huck",
    "intelligence": '0',
    "power": '7',
    "strength": '10',
    "agility": '8'
    }]

    result = find_all_characters(filename)
    #Rodar sem o arquivo characters, primeira vez
    #Daqui pra cima todos funcionam
    #assert result == expected

def test_delete_character():
    filename = "characters.csv"
    character_id = 1
    expected = True
    result = delete_character(filename, character_id)
    assert result == expected

def test_update_character():
    filename = "characters.csv"
    character_id = 1
    to_update = {"power": 10, "intelligence": 10, "ameba": 10}
    
    expected = {
    "id": '1',
    "name": "Huck",
    "intelligence": '10',
    "power": '10',
    "strength": '10',
    "agility": '8'
    }

    result = update_character(filename, character_id, **to_update)
    #assert result == expected
